package test;

import booking.model.Airport;
import booking.model.Booking;
import booking.model.Flight;
import booking.model.Passenger;
import booking.service.BookingService;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class ServiceBookingTest {

    Booking b = new Booking(
            UUID.fromString("8633a3b4-c82f-4ba5-a8f3-bc7b0a66c72c"),
            new Passenger(UUID.fromString("8633a3b4-c82f-4ba5-a8f3-bc7b0a66c73c"), "Test", "Name"),
            new ArrayList<>(),
            Collections.singletonList(Collections.singletonList(new Flight(
                    UUID.fromString("8633a3b4-c82f-4ba5-a8f3-bc7b0a66c74c"),
                    "Brussels Airlines",
                    new Airport("UKBB", "Kyiv B", "Ukraine"),
                    24 / 4 / 2023,
                    new Airport("LIEE", "Cagliari", "Italy"),
                    25 / 4 / 2023,
                    10)))
    );
    @Test
    public void createBooking() {
        BookingService service = new BookingService();
        service.create(b);
        assertEquals(1, service.getAll().size());
        assertNotEquals(2, service.getAll().size());
    }
    @Test
    public void cancelByUuid() {
        BookingService service = new BookingService();
        service.create(b);
        assertEquals(1, service.getAll().size());

        service.cancelByUuid(UUID.fromString("8633a3b4-c82f-4ba5-a8f3-bc7b0a66c72c"));
        assertEquals(0, service.getAll().size());
        assertNotEquals(1, service.getAll().size());
    }
    @Test
    public void getByPassenger() {
        BookingService service = new BookingService();
        service.create(b);
        assertEquals(1, service.getAll().size());

        String result = service.getByPassenger("Test", "Name").stream().map(String::valueOf).collect(Collectors.joining(""));
        assertEquals(b.toString(), result);
        assertNotEquals(b.toString(), "s1");
    }
    @Test
    public void getAll() {
        BookingService service = new BookingService();
        Booking b2 = new Booking(
                UUID.fromString("0033a3b4-c82f-4ba5-a8f3-bc7b0a66c72c"),
                new Passenger(UUID.fromString("8033a3b4-c82f-4ba5-a8f3-bc7b0a66c73c"), "Test2", "Name"),
                new ArrayList<>(),
                Collections.singletonList(Collections.singletonList(new Flight(
                        UUID.fromString("1633a3b4-c82f-4ba5-a8f3-bc7b0a66c74c"),
                        "Brussels Airlines",
                        new Airport("UKBB", "Kyiv B", "Ukraine"),
                        24 / 4 / 2023,
                        new Airport("LIEE", "Cagliari", "Italy"),
                        25 / 4 / 2023,
                        10)))
        );
        service.create(b);
        service.create(b2);

        assertEquals(2, service.getAll().size());
        assertNotEquals(1, service.getAll().size());
    }
    @Test
    public void setAll() {
        BookingService service = new BookingService();
        Booking b2 = new Booking(
                UUID.fromString("0033a3b4-c82f-4ba5-a8f3-bc7b0a66c72c"),
                new Passenger(UUID.fromString("8033a3b4-c82f-4ba5-a8f3-bc7b0a66c73c"), "Test2", "Name"),
                new ArrayList<>(),
                Collections.singletonList(Collections.singletonList(new Flight(
                        UUID.fromString("1633a3b4-c82f-4ba5-a8f3-bc7b0a66c74c"),
                        "Brussels Airlines",
                        new Airport("UKBB", "Kyiv B", "Ukraine"),
                        24 / 4 / 2023,
                        new Airport("LIEE", "Cagliari", "Italy"),
                        25 / 4 / 2023,
                        10)))
        );
        List<Booking> bl = new ArrayList<>();
        bl.add(b);
        bl.add(b2);

        service.setAll(bl);
        assertEquals(2, service.getAll().size());
        assertNotEquals(1, service.getAll().size());
    }
}
