package test;

import booking.controller.BaseController;
import booking.controller.BookingController;
import booking.controller.FlightController;
import booking.dao.FileDAO;
import booking.model.Airport;
import booking.model.Base;
import booking.model.Flight;
import booking.service.BaseFileService;
import booking.service.FlightServices;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static booking.doMain.base.SaveAndLoad.load;
import static booking.doMain.base.SaveAndLoad.save;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class LoadSaveFileTest {
    static BaseController baseController = new BaseController(new BaseFileService(new FileDAO<>(new File("db/base.bin"))));
    static FlightController flightController = new FlightController(new FlightServices());
    static BookingController bookingController = new BookingController();

    @Test
    public void saveInFile() {
        try {
            Files.createDirectory(Paths.get("db"));
        } catch (IOException ignored) {
        }
        save(baseController, flightController, bookingController);
        assertTrue(Files.exists(Paths.get(new File("db/base.bin").toURI())));
        assertTrue(new File("db/base.bin").delete());
        assertTrue(new File("db").delete());
    }

    @Test
    public void loadFromFile() {
        try {
            Files.createDirectory(Paths.get("db"));
        } catch (IOException ignored) {
        }
        save(baseController, flightController, bookingController);
        load(baseController, flightController, bookingController);
        List<Base> base = new ArrayList<>();
        List<Flight> flight = new ArrayList<>();

        flight.add(new Flight(UUID.fromString("5c58f38a-51ec-4cfb-9e5a-cda8e3d8dc89"),
                "Qatar Airways",
                new Airport("a", "a", "a"),
                25 / 4 /2023,
                new Airport("b", "b", "b"),
                26 / 4 /2023,
    8));

        base.add(new Base(flight, new ArrayList<>()));

        baseController.saveBase(base);

        Flight flightTest = baseController.getBase().get(0).flights().get(0);

        assertEquals(flightTest.from().country(), "a");

        assertEquals(flightTest.to().country(), "b");

        assertEquals(8, (int) flightTest.passengerCapacity());

        assertEquals(flightTest.departureDate(), 25 / 4 /2023);

        assertEquals(flightTest.arrivalDate(), 26 / 4 /2023);

        assertTrue(new File("db/base.bin").delete());

        assertTrue(new File("db").delete());
    }
}
