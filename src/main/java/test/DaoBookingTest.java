package test;

import booking.dao.CollectionBookingDAO;
import booking.model.Airport;
import booking.model.Booking;
import booking.model.Flight;
import booking.model.Passenger;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class DaoBookingTest {

    Booking b = new Booking(
            UUID.fromString("8633a3b4-c82f-4ba5-a8f3-bc7b0a66c72c"),
            new Passenger(UUID.fromString("8633a3b4-c82f-4ba5-a8f3-bc7b0a66c73c"), "Test", "Name"),
            new ArrayList<>(),
            Collections.singletonList(Collections.singletonList(new Flight(
                    UUID.fromString("8633a3b4-c82f-4ba5-a8f3-bc7b0a66c74c"),
                    "Brussels Airlines",
                    new Airport("UKBB", "Kyiv B", "Ukraine"),
                    24 / 4 / 2023,
                    new Airport("LIEE", "Cagliari", "Italy"),
                    25 / 4 / 2023,
                    10)))
    );

    @Test
    public void createBooking() {
        CollectionBookingDAO dao = new CollectionBookingDAO();
        int s0 = dao.getAll().size();
        dao.create(b);
        int s1 = dao.getAll().size();
        assertEquals(0, s0);
        assertEquals(1, s1);
        assertNotEquals(1, s0);
    }
    @Test
    public void deleteByUuid() {
        CollectionBookingDAO dao = new CollectionBookingDAO();
        dao.create(b);
        int s1 = dao.getAll().size();
        assertEquals(1, s1);

        dao.deleteByUuid(UUID.fromString("8633a3b4-c82f-4ba5-a8f3-bc7b0a66c72c"));
        int s0 = dao.getAll().size();
        assertEquals(0, s0);
        assertNotEquals(1, s0);
    }

    @Test
    public void getByPassenger() {
        CollectionBookingDAO dao = new CollectionBookingDAO();
        dao.create(b);
        int s1 = dao.getAll().size();
        assertEquals(1, s1);

        String result = dao.getByPassenger("Test", "Name").stream().map(String::valueOf).collect(Collectors.joining(""));
        assertEquals(b.toString(), result);
        assertNotEquals(b.toString(), "s1");
    }

    @Test
    public void getAll() {
        CollectionBookingDAO dao = new CollectionBookingDAO();
        Booking b2 = new Booking(
                UUID.fromString("0033a3b4-c82f-4ba5-a8f3-bc7b0a66c72c"),
                new Passenger(UUID.fromString("8033a3b4-c82f-4ba5-a8f3-bc7b0a66c73c"), "Test2", "Name"),
                new ArrayList<>(),
                Collections.singletonList(Collections.singletonList(new Flight(
                        UUID.fromString("1633a3b4-c82f-4ba5-a8f3-bc7b0a66c74c"),
                        "Brussels Airlines",
                        new Airport("UKBB", "Kyiv B", "Ukraine"),
                        24 / 4 / 2023,
                        new Airport("LIEE", "Cagliari", "Italy"),
                        25 / 4 / 2023,
                        10)))
        );
        dao.create(b);
        dao.create(b2);

        assertEquals(2, dao.getAll().size());
        assertNotEquals(1, dao.getAll().size());
    }
    @Test
    public void setAll() {
        CollectionBookingDAO dao = new CollectionBookingDAO();
        Booking b2 = new Booking(
                UUID.fromString("0033a3b4-c82f-4ba5-a8f3-bc7b0a66c72c"),
                new Passenger(UUID.fromString("8033a3b4-c82f-4ba5-a8f3-bc7b0a66c73c"), "Test2", "Name"),
                new ArrayList<>(),
                Collections.singletonList(Collections.singletonList(new Flight(
                        UUID.fromString("1633a3b4-c82f-4ba5-a8f3-bc7b0a66c74c"),
                        "Brussels Airlines",
                        new Airport("UKBB", "Kyiv B", "Ukraine"),
                        24 / 4 / 2023,
                        new Airport("LIEE", "Cagliari", "Italy"),
                        25 / 4 / 2023,
                        10)))
        );
        List<Booking> bl = new ArrayList<>();
        bl.add(b);
        bl.add(b2);

        dao.setAll(bl);
        assertEquals(2, dao.getAll().size());
        assertNotEquals(1, dao.getAll().size());
    }
}
