package test;

import org.junit.Test;

public class AllTest {
    @Test
    public void controllerBookingTest() {
        ControllerBookingTest controllerBookingTest = new ControllerBookingTest();
        controllerBookingTest.createBooking();
        controllerBookingTest.cancelByUuid();
        controllerBookingTest.getByPassenger();
        controllerBookingTest.getAll();
        controllerBookingTest.addAllBookings();
    }

    @Test
    public void daoBookingTest() {
        DaoBookingTest daoBookingTest = new DaoBookingTest();
        daoBookingTest.createBooking();
        daoBookingTest.deleteByUuid();
        daoBookingTest.getByPassenger();
        daoBookingTest.getAll();
        daoBookingTest.setAll();
    }

    @Test
    public void daoFlightTest() {
        DaoFlightTest daoFlightTest = new DaoFlightTest();
        daoFlightTest.createFlight();
        daoFlightTest.addAllFlightsTest();
        daoFlightTest.getFlightByUuidTest();
        daoFlightTest.getFlightsByDDPTest();
        daoFlightTest.getAllTest();
    }

    @Test
    public void loadSaveFileTest() {
        LoadSaveFileTest loadSaveFileTest = new LoadSaveFileTest();
        loadSaveFileTest.saveInFile();
        loadSaveFileTest.loadFromFile();
    }

    @Test
    public void serviceBookingTest() {
        ServiceBookingTest serviceBookingTest = new ServiceBookingTest();
        serviceBookingTest.createBooking();
        serviceBookingTest.cancelByUuid();
        serviceBookingTest.getByPassenger();
        serviceBookingTest.getAll();
        serviceBookingTest.setAll();
    }
}
