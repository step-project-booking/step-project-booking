package test;

import booking.controller.FlightController;
import booking.model.Flight;
import booking.service.FlightServices;
import org.junit.Test;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.UUID;

import static booking.doMain.gen.Gen.genFlights;
import static org.junit.Assert.*;

public class DaoFlightTest {
    @Test
    public void createFlight() {
        FlightController flightController = new FlightController(new FlightServices());
        Flight flight = genFlights(1).get(0);
        flightController.create(flight);
        List<Flight> flights = flightController.getAll();
        assertTrue( !flights.isEmpty());
        assertEquals(1,  flights.size());
    }
    @Test
    public void addAllFlightsTest() {
        FlightController flightController = new FlightController(new FlightServices());
        flightController.addAllFlights(genFlights(10));
        List<Flight> flights = flightController.getAll();
        assertTrue( !flights.isEmpty());
        assertEquals(10,  flights.size());
    }
    @Test
    public void getFlightByUuidTest() {
        FlightController flightController = new FlightController(new FlightServices());
        flightController.addAllFlights(genFlights(50));
        String id = String.valueOf(flightController.getAll().get(0).id());
        assertEquals(flightController.getFlightByUuid(UUID.fromString(id)).id(), UUID.fromString(id));
    }

    @Test
    public void getFlightsByDDPTest() {
        FlightController flightController = new FlightController(new FlightServices());
        flightController.addAllFlights(genFlights(100000));
        LocalDateTime localDateTime = LocalDateTime.now();
        ZonedDateTime zdt = ZonedDateTime.of(localDateTime, ZoneId.systemDefault());
        long date = zdt.toInstant().toEpochMilli();
        List<Flight> flights = flightController.getFlightsByDDP("Spain", date, 1);
        assertTrue( flights.size() > 0);
        assertEquals(flights.get(0).to().country(), "Spain");
    } @Test

    public void getAllTest() {
        int flightsCount = 10;
        FlightController flightController = new FlightController(new FlightServices());
        flightController.addAllFlights(genFlights(flightsCount));
        List<Flight> flights = flightController.getAll();
        assertTrue(!flights.isEmpty());
        assertEquals(flightsCount, flights.size());
    }
}
