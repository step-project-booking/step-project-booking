package libs;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

public class ParseDate {
    public record ParsedInfoDate(int year, int month, int day, int hour, int minute) {}
    public  static ParsedInfoDate parseLong(long date) {
        LocalDateTime localDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(date), ZoneId.systemDefault());
        int year = localDateTime.getYear();
        int month = localDateTime.getMonthValue();
        int day = localDateTime.getDayOfMonth();
        int hour = localDateTime.getHour();
        int minute = localDateTime.getMinute();
        return new ParsedInfoDate(year, month, day, hour, minute);
    }
}
