package libs;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class Timestamp {
    public static DateFormat TIMESTAMP = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
}
