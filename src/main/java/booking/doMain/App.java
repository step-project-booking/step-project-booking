package booking.doMain;

import booking.controller.BaseController;
import booking.controller.BookingController;
import booking.controller.FlightController;
import booking.dao.FileDAO;
import booking.service.BaseFileService;
import booking.service.FlightServices;
import java.io.File;
import static booking.doMain.base.SaveAndLoad.load;
import static booking.doMain.base.SaveAndLoad.save;
import static booking.doMain.menu.Cancel.cancelBooking;
import static booking.doMain.menu.My.myFlights;
import static booking.doMain.menu.OnlineBoard.onlineScoreBoard;
import static booking.doMain.menu.SearchBooking.searchAndBooking;
import static booking.doMain.menu.Validation.checkFile;
import static booking.doMain.menu.Validation.valid;
import static booking.doMain.menu.ViewFlight.ViewFlightInfo;
import static booking.view.Header.head;
import static libs.Console.ln;
import static libs.Console.nextLine;

public class App {

    static File file = checkFile(new File("db/base.bin"));
    static BaseController baseController = new BaseController(new BaseFileService(new FileDAO<>(file)));
    static FlightController flightController = new FlightController(new FlightServices());
    static BookingController  bookingController = new BookingController();

    public static void run() {
        load(baseController, flightController, bookingController);
        while (true) {
            head();
            switch (valid(nextLine())) {
                case "1" -> onlineScoreBoard(flightController);
                case "2" -> ViewFlightInfo(flightController, bookingController);
                case "3" -> searchAndBooking(flightController, bookingController);
                case "4" -> cancelBooking(flightController, bookingController);
                case "5" -> myFlights(bookingController);
                case "0" -> {
                    save(baseController, flightController, bookingController);
                    ln("program completed");
                    return;
                }
            }
        }
    }
}
