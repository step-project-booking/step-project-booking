package booking.doMain.gen;

import booking.model.Airport;
import booking.model.Flight;
import libs.ParseDate.ParsedInfoDate;

import java.sql.Timestamp;
import java.time.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static booking.doMain.gen.dataGen.AirlineData.airline;
import static booking.doMain.gen.dataGen.AirportData.*;
import static booking.doMain.gen.dataGen.AirportData.country;
import static libs.MethodMath.random;
import static libs.ParseDate.parseLong;

public class Gen {
    public static List<Flight> genFlights(int k) {
        List<Flight> flights = new ArrayList<>();

        for (int i = 0; i < k; i++) {
            long start = Timestamp.valueOf(LocalDateTime.now()).getTime() + (random(0, (int) (8.64 * Math.pow(10, 7))));
            ParsedInfoDate sPid = parseLong(start);

            long finish = start + (int) (4.32 * Math.pow(10, 7)) + (random(0, (int) (8.64 * Math.pow(10, 7))));
            ParsedInfoDate fPid = parseLong(finish);
            int fromAirport = random(0, 238);
            int toAirport = random(0, 238);

            flights.add(new Flight(UUID.randomUUID(),
                    airline[random(0, 99)],
                    new Airport(icao[fromAirport], airport[fromAirport], country[fromAirport]),
                    getTimestamp(sPid.year(), sPid.month(), sPid.day(), sPid.hour(),getMinutesRandom(sPid.minute())),
                    new Airport(icao[toAirport], airport[toAirport], country[toAirport]),
                    getTimestamp(fPid.year(), fPid.month(), fPid.day(), fPid.hour(),getMinutesRandom(sPid.minute())),
                    random(0, 10)
            ));
        }
        return flights;
    }

    public static long getTimestamp(int... a) {
        return Timestamp.valueOf(LocalDateTime.of(a[0], a[1], a[2], a[3], a[4])).getTime();
    }

    public static int getMinutesRandom(int a) {
        int b = 0;
        if (a > 10) b = 15;
        if (a > 25) b = 30;
        if (a > 40) b = 45;
        return b;
    }
}
