package booking.doMain.menu;

import booking.Exception.ExceptionNotFound;
import booking.controller.BookingController;
import booking.controller.FlightController;

import java.util.NoSuchElementException;
import java.util.UUID;

import static libs.Console.*;

public class ViewFlight {

    public static void ViewFlightInfo(FlightController fc, BookingController bc) {
        viewCancel(fc, bc, true);
    }

    public static void viewCancel(FlightController fc, BookingController bc, boolean b) {
        String gn = "enter id or '0' to exit to the main menu";
        String nsl = "id not found\n" + gn;
        String nf = "incorrect format\n" + gn;
        String lg = "length must be 36 characters\n" + gn;
        ln("enter id | example: ee3b5861-b746-4ffb-a30e-b67104e5c68f");
        ln("'0' to exit ");
        while (true) {
            String id = nextLine();
            if (id.equals("0")) {
                return;
            }
            if (id.length() == 36) {
                try {
                    if (b) {
                        ln(fc.getFlightByUuid(UUID.fromString(id)));
                    } else if (bc.cancelByUuid(UUID.fromString(id))) {
                        ln("Booking canceled");
                    }
                } catch (NoSuchElementException e) {
                    ln(nsl);
                    continue;
                } catch (NumberFormatException ne) {
                    ln(nf);
                    continue;
                } catch (ExceptionNotFound enf) {
                    ln(enf);
                }
                return;
            } else {
                ln(lg);
            }
        }
    }
}
