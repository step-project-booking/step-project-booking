package booking.doMain.menu;

import booking.Exception.ExceptionIdRange;
import booking.controller.BookingController;
import booking.controller.FlightController;
import booking.model.Booking;
import booking.model.Flight;
import booking.model.Passenger;
import libs.Timestamp;

import java.text.ParseException;
import java.util.*;
import static booking.doMain.menu.Validation.isValidDateFormat;
import static libs.Console.*;

public class SearchBooking {

    public static void searchAndBooking(FlightController fc, BookingController bc) {
        while (true) {
            String[] chatSearchArray = chatSearch();
            List<List<Flight>> resultFlights = resultChatSearch(chatSearchArray, fc);

            if (resultFlights == null) {
                return;
            }

            if (resultFlights.size() == 0) {
                ln("No flights found");
            } else {
                for(List<Flight> lf: resultFlights){
                    if(lf.size() == 2){
                        ln("\u001B[32m--------------------------------------- flight with transfer ---------------------------------------\u001B[0m");
                    } else {
                        ln("\u001B[32m------------------------------------------ direct flight -------------------------------------------\u001B[0m");
                    }

                    for(Flight f: lf){
                        ln(f);
                    }
                }

                ln("\nWant to make a reservation? | yes/no or '0' - back to main menu");
                ln("\u001B[32m-----------------------------------------------------------------------------------------------------\u001B[0m");
                switch (nextLine()) {
                    case "yes" -> booking(resultFlights, chatSearchArray, bc);
                    case "0" -> {
                        return;
                    }
                }
            }
        }
    }

    static String bookLine(int passenger, String responsible) {
        String line = "\u001B[32m-----------------------------------------------------------------------------------------------------\u001B[0m";
        return String.format("\nEnter the first and last name separated by a space for passenger %d %s\nExample: Name Surname\n", passenger, responsible) + line;
    }

    static void booking(List<List<Flight>> flight, String[] chatSearchArray, BookingController bc) {
        int serialNumber = getSerialNumber(flight.size());

        List<Passenger> passengers = new ArrayList<>();
        String[] passenger, admin = new String[0];

        for (int i = 0; i < Integer.parseInt(chatSearchArray[2]); i++) {
            if (i == 0) {
                admin = getPassenger(1, "(Admin)");
            } else {
                passenger = getPassenger(i + 1, "");
                passengers.add(new Passenger(UUID.randomUUID(), passenger[0], passenger[1]));
            }
        }

        List<List<Flight>> flights = new ArrayList<>();
        flights.add(flight.get(serialNumber - 1));

        Booking booking = new Booking(UUID.randomUUID(),
                new Passenger(UUID.randomUUID(), admin[0], admin[1]),
                passengers,
                flights);

        bc.createBooking(booking);

        ln("\nBooking successful!\n");
        ln(booking);
    }

    static int getSerialNumber(int id) {
        int serialNumber;
        while (true) {
            ln("\nSelect a flight. Enter serial number");
            ln("\u001B[32m-----------------------------------------------------------------------------------------------------\u001B[0m");
            String sn = nextLine();
            try {
                int psn = Integer.parseInt(sn);
                if (psn > id || psn < 1) {
                    throw new ExceptionIdRange(String.format("%d Value not within found flights!\n", psn));
                }
                serialNumber = psn;
                break;
            } catch (NumberFormatException e) {
                f("'%s' not a number", sn);
            } catch (ExceptionIdRange er) {
                ln(er);
            }
        }
        return serialNumber;
    }

    static String[] getPassenger(int passenger, String responsible) {
        while (true) {
            ln(bookLine(passenger, responsible));
            String[] p = nextLine().split(" ");
            if (p.length != 2) {
                ln("Invalid input format");
            } else return p;
        }
    }

    static List<List<Flight>> resultChatSearch(String[] resultChatSearch, FlightController fc) {
        long dateLong = 0;

        if (Objects.equals(resultChatSearch[0], "0")) {
            return null;
        }
        try {
            dateLong = Timestamp.TIMESTAMP.parse(resultChatSearch[1] + " " + "00:00:00").getTime();
        } catch (ParseException e) {
            ln(e);
        }

        return fc.getListOfFlightsList(resultChatSearch[0], dateLong, Integer.parseInt(resultChatSearch[2]));
    }

    static String[] chatSearch() {
        String destination, date, passengers = null, m = "Value cannot be empty!";

        ln("\nFlight search and booking. | '0' - back to main menu");
        ln("\u001B[32m-----------------------------------------------------------------------------------------------------\u001B[0m");

        while (true) {
            ln("Enter country of destination");
            destination = nextLine();
            if (destination.isEmpty()) {
                ln(m);
                continue;
            }
            if (Objects.equals(destination, "0")) {
                return new String[]{"0"};
            }
            break;
        }

        while (true) {
            ln("Enter date dd/MM/yyyy");
            date = nextLine();
            if (date.isEmpty()) {
                ln(m);
                continue;
            }
            if (Objects.equals(date, "0")) {
                return new String[]{"0"};
            }
            if (!isValidDateFormat(date)) {
                ln("Invalid date value!");
            } else break;
        }

        while (true) {
            ln("How many passengers?");
            try {
                passengers = nextLine();
                if (passengers.isEmpty()) {
                    ln(m);
                    continue;
                }
                if (Objects.equals(passengers, "0")) {
                    return new String[]{"0"};
                }
                Integer.parseInt(passengers);
                break;
            } catch (NumberFormatException e) {
                f("'%' not a number!", passengers);
            }
        }

        return new String[]{destination, date, passengers};
    }
}
