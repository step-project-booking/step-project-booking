package booking.doMain.menu;

import booking.Exception.ExceptionNotFound;
import booking.controller.BookingController;
import booking.model.Booking;
import libs.Console;

import java.util.List;
import java.util.Objects;

import static libs.Console.*;

public class My {

    public static void myFlights(BookingController bc) {
        while (true) {
            String line = "\u001B[32m-----------------------------------------------------------------------------------------------------\u001B[0m";
            ln("\nEnter the first and last name separated by a space\nExample: Name Surname | '0' - back to main menu\n" + line);
            String[] p = nextLine().split(" ");
            if (Objects.equals(p[0], "0")) return;
            if (p.length != 2) {
                ln("Invalid input format");
            } else {
                try {
                    List<Booking> b = bc.getByPassenger(p[0], p[1]);
                    b.forEach(Console::ln);
                    return;
                } catch (ExceptionNotFound e) {
                    ln(e);
                }
            }
        }
    }
}

