package booking.doMain.menu;

import booking.controller.BookingController;
import booking.controller.FlightController;

import static booking.doMain.menu.ViewFlight.viewCancel;

public class Cancel {
    public static void cancelBooking(FlightController fc, BookingController bc){
        viewCancel(fc,bc, false);
    }
}
