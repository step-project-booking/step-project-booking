package booking.doMain.menu;

import booking.Exception.ExceptionNotFoundNotCreatedFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import static libs.Console.f;

public class Validation {
    public static String valid(String a){
        int n = 0;
        try {
           n = Integer.parseInt(a);
        }catch (NumberFormatException e){
            f("'%s'- not a number\n", a);
            a = "";
        }
        if(n < 0 || n > 6){
            f("'%s'- there is no such command\n", a);
            a = "";
        }
        return a;
    }

    public static File checkFile(File file){
        if (Files.exists(Paths.get(file.toURI()))) {
            return file;
        } else {
            try {
                Files.createDirectory(Paths.get("db"));
                file = Files.createFile(Path.of("db/base.bin")).toFile();
            } catch (IOException e) {
                throw new ExceptionNotFoundNotCreatedFile (e);
            }
        }
        return file;
    }

    public static boolean isValidDateFormat(String dateString) {
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

        try {
            LocalDate.parse(dateString, dateFormatter);
            return true;
        } catch (DateTimeParseException e) {
            return false;
        }
    }
}
