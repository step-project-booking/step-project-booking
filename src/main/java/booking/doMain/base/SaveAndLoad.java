package booking.doMain.base;

import booking.controller.BaseController;
import booking.controller.BookingController;
import booking.controller.FlightController;
import booking.model.Base;

import java.util.ArrayList;
import java.util.List;

import static booking.doMain.gen.Gen.genFlights;
import static libs.Console.ln;

public class SaveAndLoad {

    public static void load(BaseController b, FlightController fc, BookingController bc) {
        Base base;

        if (b.getBase() != null) {
            base = b.getBase().get(0);
            fc.addAllFlights(base.flights());
            bc.addAllBookings(base.bookings());
        }

        int size = fc.getAll().size();

        if (size == 0) {
            fc.addAllFlights(genFlights(20000));
            ln("\u001B[33mLoading base done from generation.\u001B[0m");
        }

        if (size != 0) {
            ln("\u001B[33mThe database was loaded from a file.\u001B[0m");
        }
    }

    public static void save(BaseController b, FlightController fc, BookingController bc) {
        Base base = new Base(fc.getAll(), bc.getAll());
        List<Base> lb = new ArrayList<>();
        lb.add(base);
        b.saveBase(lb);
    }
}
