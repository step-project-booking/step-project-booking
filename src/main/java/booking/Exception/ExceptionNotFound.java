package booking.Exception;

public class ExceptionNotFound extends RuntimeException{
    public ExceptionNotFound(String message) {
        super(message);
    }


}
