package booking.Exception;

public class ExceptionIdRange extends RuntimeException{
    public ExceptionIdRange(String message) {
        super(message);
    }
}
