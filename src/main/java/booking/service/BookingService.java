package booking.service;

import booking.dao.CollectionBookingDAO;
import booking.model.Booking;

import java.util.List;
import java.util.UUID;

public class BookingService {
    private final CollectionBookingDAO dao;

    public BookingService() { this.dao = new CollectionBookingDAO(); }

    public List<Booking> getAll() { return dao.getAll(); }

    public List<Booking> getByPassenger(String name, String surName) { return dao.getByPassenger(name, surName); }

    public boolean cancelByUuid(UUID id) { return dao.deleteByUuid(id); }

    public void create(Booking booking) {
        dao.create(booking);
    }

    public void setAll(List<Booking> b) { dao.setAll(b); }
}
