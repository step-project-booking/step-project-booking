package booking.service;

import booking.dao.FileDAO;
import booking.model.Base;


import java.util.List;

public class BaseFileService {
    private final FileDAO<Base> dao;

    public BaseFileService(FileDAO<Base> dao) {
        this.dao = dao;
    }

    public void saveBase(List<Base> base) {
        dao.saveBase(base);
    }

    public List<Base> getAll() {
        return dao.getAll();
    }
}

