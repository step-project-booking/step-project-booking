package booking.service;

import booking.dao.CollectionFlightsDAO;
import booking.dao.interfaces.FlightsDAO;
import booking.model.Flight;

import java.util.List;
import java.util.UUID;

public class FlightServices implements FlightsDAO<Flight> {

    private final FlightsDAO<Flight> flights = new CollectionFlightsDAO<>();

    public List<Flight> getFlightsByDDP(String destination, long date, int passengers) {
        return flights.getFlightsByDDP(destination, date, passengers);
    }

    public List<List<Flight>> getListOfFlightsList(String to, long date, int passengers){
        return flights.getListOfFlightsList(to, date, passengers);
    }

    @Override
    public Flight getFlightByUuid(UUID uuid) {
        return flights.getFlightByUuid(uuid);
    }

    @Override
    public void addAllFlights(List<Flight> flightsToAdd) {
        flights.addAllFlights(flightsToAdd);
    }

    @Override
    public List<Flight> getAll() {
        return flights.getAll();
    }

    @Override
    public boolean create(Flight flight) {
        return flights.create(flight);
    }
}
