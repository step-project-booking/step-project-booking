package booking.model;

import java.io.Serializable;
import java.util.UUID;

public record Passenger (
    UUID id,
    String firstName,
    String lastName
) implements Serializable {
    @Override
    public String toString() {
        return String.format("%s %s", this.firstName, this.lastName);
    }
    @Override
    public int hashCode() {
        return firstName.hashCode() + lastName.hashCode() + 37;
    }
    @Override
    public boolean equals (Object obj) {
        if (obj == null || getClass() != obj.getClass()) return false;
        if (this == obj) return true;

        if (!(obj instanceof Passenger p)) return false;

        return this.id().equals(p.id()) &&
                this.firstName().equals(p.firstName()) &&
                this.lastName().equals(p.lastName());
    }
}
