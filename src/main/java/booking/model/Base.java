package booking.model;

import java.io.Serializable;
import java.util.List;

public record Base(List <Flight> flights, List<Booking> bookings) implements Serializable {
}
