package booking.model;
import booking.dao.interfaces.FlightData;
import java.io.Serializable;
import static libs.Timestamp.TIMESTAMP;
import java.util.UUID;

public record Flight(
        UUID id,
        String airline,
        Airport from,
        long departureDate,
        Airport to,
        long arrivalDate,
        Integer passengerCapacity
) implements Serializable, FlightData {

    @Override
    public String toString() {
        return String.format("""
                        \nid: %s
                        \u001B[36m\t\tairline: %s\u001B[0m
                        \t\tfrom: %s \t\tto: %s\s
                        \t\tdeparture: %s \t\t\t\t\tarrival: %s\s
                        \t\tfree places: %s\s
                        """,
                id, airline, from, to, TIMESTAMP.format(departureDate),
                TIMESTAMP.format(arrivalDate), passengerCapacity
        );
    }
}