package booking.model;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public record Booking(
        UUID id,
        Passenger admin,
        List<Passenger> passengers,
        List<List<Flight>> flight

) implements Serializable{
    private String passengerstoString(Passenger admin, List<Passenger> passengers) {
        if(passengers.size() > 0) {
            String p = passengers.stream().map(String::valueOf).collect(Collectors.joining(", "));
            return  String.format("%s, %s", admin, p);
        }
        return admin.toString();
    }

    private String flightToString(List<List<Flight>> flight) {
        return flight.stream()
                .map(String::valueOf)
                .collect(Collectors.joining("\n"))
                .replaceAll("^\\[|\\]$", "\n\u001B[33m-----------------------------------------------------------------------------------------------------\u001B[0m")
                .replaceAll(",","");
    }

    @Override
    public String toString() {
        return String.format("Your booking number: %s\nPassengers: %s\nFlight information: %s", this.id, passengerstoString(this.admin, this.passengers), flightToString(this.flight));
    }
    @Override
    public int hashCode() {
        return flight.hashCode() + passengers.hashCode() + id.hashCode() + 19;
    }

    @Override
    public boolean equals (Object obj) {
        if (obj == null || getClass() != obj.getClass()) return false;
        if (this == obj) return true;

        if (!(obj instanceof Booking b)) return false;

        return this.id().equals(b.id()) &&
                this.admin().equals(b.admin()) &&
                this.passengers().equals(b.passengers()) &&
                this.flight().equals(b.flight());
    }
}
