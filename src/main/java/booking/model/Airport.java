package booking.model;

import java.io.Serializable;

public record Airport(String icao, String airport, String country) implements Serializable {
    @Override
    public String toString() {
        return icao + "\t" + airport + "\t" + country;
    }
}
