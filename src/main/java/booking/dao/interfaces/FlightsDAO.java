package booking.dao.interfaces;

import java.util.List;
import java.util.UUID;

public interface FlightsDAO<A> extends DAO<A> {

    A getFlightByUuid(UUID id);

    List<A> getFlightsByDDP(String destination, long date, int passengers);

    void addAllFlights(List<A> flightsToAdd);

    List<List<A>> getListOfFlightsList(String to, long date, int passengers);
}
