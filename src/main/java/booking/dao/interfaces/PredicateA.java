package booking.dao.interfaces;

public interface PredicateA<T, F, N extends Number> {
    boolean test(T a, F b, N c);
}
