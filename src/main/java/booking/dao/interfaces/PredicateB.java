package booking.dao.interfaces;

public interface PredicateB<T, L extends Number> {
    boolean test(T t, L d);
}
