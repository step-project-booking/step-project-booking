package booking.dao.interfaces;

import booking.model.Airport;

import java.util.UUID;

public interface FlightData {

    UUID id();

    Airport to();

    Airport from();

    long departureDate();

    long arrivalDate();

    Integer passengerCapacity();
}
