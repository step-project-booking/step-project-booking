package booking.dao;

import booking.dao.interfaces.PredicateA;
import booking.dao.interfaces.PredicateB;
import booking.model.Flight;
import libs.Timestamp;

public class Lambda {
    public static PredicateA<Flight, Flight, Integer> transDateDiff =
            (fromFlight, toFlight, passengers) -> fromFlight.arrivalDate() + 72 * Math.pow(10, 5) < toFlight.departureDate()
            && toFlight.departureDate() < fromFlight.arrivalDate() + 432 * Math.pow(10, 5) && toFlight.passengerCapacity() >= passengers;
    public static PredicateB<Flight, Long> flyingAway = (x1, date) ->
            Timestamp.TIMESTAMP.format(x1.departureDate()).split(" ")[0]
            .equals(Timestamp.TIMESTAMP.format(date).split(" ")[0]);
}
