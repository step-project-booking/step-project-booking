package booking.dao;

import booking.Exception.ExceptionNotFound;
import booking.dao.interfaces.FlightData;
import booking.dao.interfaces.FlightsDAO;
import booking.model.Flight;
import libs.Timestamp;
import java.util.*;
import java.util.stream.Stream;

import static booking.dao.Lambda.flyingAway;
import static booking.dao.Lambda.transDateDiff;

public class CollectionFlightsDAO<T extends FlightData> implements FlightsDAO<T> {

    private final List<T> flights = new ArrayList<>();

    public List<T> getFlightsByDDP(String country, long date, int passengers) {
        return flights.stream()
                .filter(x1 -> x1.from().country().equals("Ukraine") &&
                        x1.passengerCapacity() >= passengers &&
                        x1.to().country().equals(country) &&
                        Timestamp.TIMESTAMP.format(x1.departureDate()).split(" ")[0]
                            .equals(Timestamp.TIMESTAMP.format(date).split(" ")[0]))
                                .toList();
    }

    public List<List<T>> getListOfFlightsList(String country, long date, int passengers) {
        return Stream.concat(
                this.flights.stream()
                        .filter(x1 -> x1.from().country().equals("Ukraine") && x1.passengerCapacity() >= passengers)
                        .filter(x1 -> flyingAway.test((Flight) x1, date))
                        .flatMap(fromFlight -> this.flights.stream()
                                .filter(y -> y.to().country().equals(country))
                                .filter(toFlight -> fromFlight.to().country().equals(toFlight.from().country()))
                                .filter(toFlight -> transDateDiff.test((Flight) fromFlight, (Flight) toFlight, passengers))
                                .map(toFlight -> Arrays.asList(fromFlight, toFlight))
                        ),
                getFlightsByDDP(country, date, passengers).stream()
                        .map(Collections::singletonList)
        ).toList();
    }

    @Override
    public T getFlightByUuid(UUID id) {
        Optional<T> flightStream = flights.stream().filter(x -> x.id().equals(id)).findFirst();
        if(flightStream.isPresent()) return flightStream.get();
        throw new ExceptionNotFound("no such element found by uuid");
    }

    @Override
    public void addAllFlights(List<T> flightsToAdd) {
        flights.addAll(flightsToAdd);
    }

    @Override
    public List<T> getAll() {
        return flights;
    }

    @Override
    public boolean create(T t) {
        flights.add(t);
        return true;
    }
}
