package booking.dao;

import booking.dao.interfaces.DAO;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import static libs.Console.f;
import static libs.Console.ln;
import static libs.EX.NI;

public class FileDAO<T extends Serializable> implements DAO<T> {

    private final File file;

    public FileDAO(File file) {
        this.file = file;
    }

    @Override
    public List<T> getAll() {
        ArrayList<T> as = null;
        try {
            FileInputStream fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);
            as = (ArrayList<T>) ois.readObject();
            ois.close();
        } catch (IOException | ClassNotFoundException | NullPointerException e) {
            ln("\u001B[31mThe file is empty or missing!\u001B[0m");
        }
        return as;
    }

    public void saveBase(List<T> as) {
        try {
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(as);
            oos.close();
        } catch (IOException e) {
            f("\u001B[31mAn error occurred while writing the collection to a file %s\n\u001B[0m", e);
        }
    }

    @Override
    public boolean create(T t) {
        throw NI;
    }
}