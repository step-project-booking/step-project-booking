package booking.dao;

import booking.Exception.ExceptionNotFound;
import booking.dao.interfaces.DAO;
import booking.model.Booking;
import java.util.*;

public class CollectionBookingDAO implements DAO<Booking> {
    private final List<Booking> bookingList = new ArrayList<>();

    @Override
    public List<Booking> getAll() {
        return bookingList;
    }

    @Override
    public boolean create(Booking booking) {
        if(booking == null) return false;
        if(bookingList.contains(booking)) return false;
        bookingList.add(booking);
        return true;
    }

    public void setAll(List<Booking> allBookings) {
        bookingList.addAll(allBookings);
    }

    public List<Booking> getByPassenger(String name, String surName) throws ExceptionNotFound {
        List<Booking> all = bookingList.stream()
                .filter(booking ->
                        booking.admin().firstName().equals(name) && booking.admin().lastName().equals(surName) ||
                                booking.passengers().stream().anyMatch(passenger ->
                                        passenger.firstName().equals(name) && passenger.lastName().equals(surName))
                ).toList();
        if(all.size() == 0) throw new ExceptionNotFound("You don’t have bookings, add a flight");
        return all;
    }

    public boolean deleteByUuid(UUID id) throws ExceptionNotFound {
        List<Booking> all = bookingList;
        if(all.size() == 0) throw new ExceptionNotFound("You don’t have bookings");
        if(all.stream().noneMatch(x -> x.id().equals(id))) throw new ExceptionNotFound("You don't have booking with this id");
        return bookingList.removeIf(x -> x.id().equals(id));
    }
}
