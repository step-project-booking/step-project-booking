package booking.view;

import static libs.Console.f;
import static libs.Console.ln;

public class Header {

    public static void head() {
        f("\n\u001B[32m=========================================\u001B[36m %S \u001B[0m\u001B[32m============================================\n\u001B[0m", "Flight booking");
        for (int j = 0; j < 6; j += 2) {
            f("  %-48s   %s\n", points[j], points[j + 1]);
        }
        ln("\u001B[32m=====================================================================================================\u001B[0m");
    }

    public static final String[] points = new String[]
            {
                    "-1. Online scoreboard",
                    "-2. View flight information",
                    "-3. Flight search and booking",
                    "-4. Cancel booking",
                    "-5. My flights",
                    "-0. Exit",
            };
}
