package booking.controller;

import booking.dao.interfaces.FlightsDAO;
import booking.model.Flight;
import booking.service.FlightServices;
import java.util.List;
import java.util.UUID;

public class FlightController implements FlightsDAO<Flight> {

    private final FlightServices flightServices;

    public FlightController(FlightServices bookingFlightServices) {
        this.flightServices = bookingFlightServices;
    }

    public Flight getFlightByUuid(UUID id) {
        return flightServices.getFlightByUuid(id);
    }

    @Override
    public List<Flight> getFlightsByDDP(String destination, long date, int passengers) {
        return flightServices.getFlightsByDDP(destination,date,passengers);
    }

    @Override
    public void addAllFlights(List<Flight> flightsToAdd) {
        flightServices.addAllFlights(flightsToAdd);
    }

    @Override
    public List<List<Flight>> getListOfFlightsList(String to, long date, int passengers) {
            return flightServices.getListOfFlightsList(to, date, passengers);
    }

    @Override
    public List<Flight> getAll() {
        return flightServices.getAll();
    }

    @Override
    public boolean create(Flight flight) {
        return flightServices.create(flight);
    }
}
