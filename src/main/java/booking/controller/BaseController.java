package booking.controller;

import booking.model.Base;
import booking.service.BaseFileService;

import java.util.List;

public class BaseController {
    public BaseFileService service;

    public BaseController(BaseFileService service) {
        this.service = service;
    }

    public void saveBase(List<Base> base) {
        service.saveBase(base);
    }

    public List<Base> getBase() {
        return service.getAll();
    }
}
