package booking.controller;

import booking.model.Booking;
import booking.service.BookingService;

import java.util.List;
import java.util.UUID;

public class BookingController {

    private final BookingService service;

    public BookingController() {
        this.service = new BookingService();
    }

    public List<Booking> getAll() {
        return service.getAll();
    }

    public List<Booking> getByPassenger(String name, String surName) {
        return service.getByPassenger(name, surName);
    }

    public void createBooking(Booking booking) {
        service.create(booking);
    }

    public boolean cancelByUuid(UUID id) {
        return service.cancelByUuid(id);
    }

    public void addAllBookings(List<Booking> b) {
        service.setAll(b);
    }
}
